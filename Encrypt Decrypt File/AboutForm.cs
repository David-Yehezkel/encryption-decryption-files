﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Encrypt_Decrypt_File
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
            
        }

        private void linkToBB_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/David-Yehezkel/encryption-decryption-files/src/master/");
        }

        /// <summary>
        /// represent all visual color changes for normal color mode
        /// </summary>
        public void moveNormalColorMode()
        {
            BackColor = SystemColors.Control;
            ForeColor = SystemColors.ControlText;
            aboutTextBox.BackColor = SystemColors.Control;
            aboutTextBox.ForeColor = SystemColors.ControlText;
        }

        /// <summary>
        /// represent all visual color changes for Dark Color Mode
        /// </summary>
        public void moveDarkColorMode()
        {
            BackColor = Color.Black;
            ForeColor = Color.White;
            aboutTextBox.BackColor = Color.Black;
            aboutTextBox.ForeColor = Color.White;
        }

    }
}
