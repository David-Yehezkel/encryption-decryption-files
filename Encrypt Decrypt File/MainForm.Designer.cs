﻿namespace Encrypt_Decrypt_File
{
    partial class EncDec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EncDec));
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.fileSelectorBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pathInput = new System.Windows.Forms.TextBox();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.encryptRadio = new System.Windows.Forms.RadioButton();
            this.decryptRadio = new System.Windows.Forms.RadioButton();
            this.activateBtn = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.AppearanceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darkModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creatorLabel = new System.Windows.Forms.Label();
            this.fileStatusLabel = new System.Windows.Forms.Label();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // fileSelectorBtn
            // 
            this.fileSelectorBtn.Location = new System.Drawing.Point(296, 45);
            this.fileSelectorBtn.Name = "fileSelectorBtn";
            this.fileSelectorBtn.Size = new System.Drawing.Size(75, 23);
            this.fileSelectorBtn.TabIndex = 0;
            this.fileSelectorBtn.Text = "Select File";
            this.fileSelectorBtn.UseVisualStyleBackColor = true;
            this.fileSelectorBtn.Click += new System.EventHandler(this.openFileDialogBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            // 
            // pathInput
            // 
            this.pathInput.Location = new System.Drawing.Point(77, 46);
            this.pathInput.Name = "pathInput";
            this.pathInput.ReadOnly = true;
            this.pathInput.Size = new System.Drawing.Size(200, 20);
            this.pathInput.TabIndex = 3;
            // 
            // passwordInput
            // 
            this.passwordInput.Location = new System.Drawing.Point(77, 75);
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.Size = new System.Drawing.Size(200, 20);
            this.passwordInput.TabIndex = 4;
            // 
            // encryptRadio
            // 
            this.encryptRadio.AutoSize = true;
            this.encryptRadio.Location = new System.Drawing.Point(124, 116);
            this.encryptRadio.Name = "encryptRadio";
            this.encryptRadio.Size = new System.Drawing.Size(61, 17);
            this.encryptRadio.TabIndex = 5;
            this.encryptRadio.TabStop = true;
            this.encryptRadio.Text = "Encrypt";
            this.encryptRadio.UseVisualStyleBackColor = true;
            // 
            // decryptRadio
            // 
            this.decryptRadio.AutoSize = true;
            this.decryptRadio.Location = new System.Drawing.Point(191, 116);
            this.decryptRadio.Name = "decryptRadio";
            this.decryptRadio.Size = new System.Drawing.Size(62, 17);
            this.decryptRadio.TabIndex = 6;
            this.decryptRadio.TabStop = true;
            this.decryptRadio.Text = "Decrypt";
            this.decryptRadio.UseVisualStyleBackColor = true;
            // 
            // activateBtn
            // 
            this.activateBtn.Location = new System.Drawing.Point(296, 74);
            this.activateBtn.Name = "activateBtn";
            this.activateBtn.Size = new System.Drawing.Size(75, 23);
            this.activateBtn.TabIndex = 7;
            this.activateBtn.Text = "Activate";
            this.activateBtn.UseVisualStyleBackColor = true;
            this.activateBtn.Click += new System.EventHandler(this.activateBtn_Click);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AppearanceMenuItem,
            this.aboutToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(384, 24);
            this.menu.TabIndex = 8;
            this.menu.Text = "menuStrip1";
            // 
            // AppearanceMenuItem
            // 
            this.AppearanceMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.AppearanceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.darkModeToolStripMenuItem});
            this.AppearanceMenuItem.Name = "AppearanceMenuItem";
            this.AppearanceMenuItem.Size = new System.Drawing.Size(82, 20);
            this.AppearanceMenuItem.Text = "Appearance";
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // darkModeToolStripMenuItem
            // 
            this.darkModeToolStripMenuItem.Name = "darkModeToolStripMenuItem";
            this.darkModeToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.darkModeToolStripMenuItem.Text = "Dark Mode";
            this.darkModeToolStripMenuItem.Click += new System.EventHandler(this.darkModeToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // creatorLabel
            // 
            this.creatorLabel.AutoSize = true;
            this.creatorLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.creatorLabel.Location = new System.Drawing.Point(120, 184);
            this.creatorLabel.Name = "creatorLabel";
            this.creatorLabel.Size = new System.Drawing.Size(131, 13);
            this.creatorLabel.TabIndex = 9;
            this.creatorLabel.Text = "Create By David Yehezkel";
            // 
            // fileStatusLabel
            // 
            this.fileStatusLabel.AutoSize = true;
            this.fileStatusLabel.Location = new System.Drawing.Point(120, 152);
            this.fileStatusLabel.Name = "fileStatusLabel";
            this.fileStatusLabel.Size = new System.Drawing.Size(65, 13);
            this.fileStatusLabel.TabIndex = 10;
            this.fileStatusLabel.Text = "File Status : ";
            // 
            // EncDec
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(384, 211);
            this.Controls.Add(this.fileStatusLabel);
            this.Controls.Add(this.creatorLabel);
            this.Controls.Add(this.activateBtn);
            this.Controls.Add(this.decryptRadio);
            this.Controls.Add(this.encryptRadio);
            this.Controls.Add(this.passwordInput);
            this.Controls.Add(this.pathInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fileSelectorBtn);
            this.Controls.Add(this.menu);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.Name = "EncDec";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encrypt/Decrypt Files";
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button fileSelectorBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathInput;
        private System.Windows.Forms.TextBox passwordInput;
        private System.Windows.Forms.RadioButton encryptRadio;
        private System.Windows.Forms.RadioButton decryptRadio;
        private System.Windows.Forms.Button activateBtn;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem AppearanceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darkModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label creatorLabel;
        private System.Windows.Forms.Label fileStatusLabel;
    }
}

