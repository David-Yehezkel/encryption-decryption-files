﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Encrypt_Decrypt_File
{
    public partial class EncDec : Form
    {
        private bool isDarkMode;
        private AboutForm aboutForm;
        private string filePath;
      
        public EncDec()
        {
            InitializeComponent();

        }

        /// <summary>
        /// handle open file dialog if file not exists will set empy string at path TextInput 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileDialogBtn_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            filePath = openFileDialog.FileName;
            if (!File.Exists(filePath))
                pathInput.Text = "";
            else
                pathInput.Text = filePath;
        }

        /// <summary>
        /// handle all logic under Activate button
        /// 1.check if path input is full with exists file.
        /// 2.check if password length is 8.
        /// 3.check if radio is checked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void activateBtn_Click(object sender, EventArgs e)
        {
            if (pathInput.Text.Equals(""))
                MessageBox.Show("You have to choose any file.");

            else if (passwordInput.Text.Length != 8)
                MessageBox.Show("You have to enter password with 8 characters");

            else if (encryptRadio.Checked)
            {
                Program.EncryptFile(filePath, passwordInput.Text);
                fileStatusLabel.Text = "File Status : Encrypt";
            }
            else if (decryptRadio.Checked)
            {
                Program.DecryptFile(filePath, passwordInput.Text);
                fileStatusLabel.Text = "File Status : Decrypt";
            }
            else
                MessageBox.Show("You have to choose either to Encrypt File or Decrypt File.");
        }

        /// <summary>
        /// normal tool menu handle GUI swicth to normal visual user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isDarkMode = false;
            BackColor = SystemColors.Control;
            ForeColor = SystemColors.ControlText;

            menu.BackColor = SystemColors.Control;
            menu.ForeColor = SystemColors.ControlText;

            normalToolStripMenuItem.BackColor = SystemColors.Control;
            normalToolStripMenuItem.ForeColor = SystemColors.ControlText;

            darkModeToolStripMenuItem.BackColor = SystemColors.Control;
            darkModeToolStripMenuItem.ForeColor = SystemColors.ControlText;

            creatorLabel.ForeColor = SystemColors.ControlText;

            if (aboutForm != null && CheckOpened(aboutForm.Name))
            {
                aboutForm.moveNormalColorMode();
            }

            menu.Renderer = new ToolStripProfessionalRenderer();

        }

        /// <summary>
        /// dark mode tool menu handle GUI swicth to dark visual mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void darkModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isDarkMode = true;
            BackColor = Color.Black;
            ForeColor = Color.White;

            fileSelectorBtn.ForeColor = Color.Black;
            activateBtn.ForeColor = Color.Black;

            menu.BackColor = Color.Black;
            menu.ForeColor = Color.White;

            normalToolStripMenuItem.BackColor = Color.Black;
            normalToolStripMenuItem.ForeColor = Color.White;

            darkModeToolStripMenuItem.BackColor = Color.Black;
            darkModeToolStripMenuItem.ForeColor = Color.White;

            creatorLabel.ForeColor = Color.White;

            menu.Renderer = new DarkRenderer();

            if (aboutForm!=null && CheckOpened(aboutForm.Name))
            {
                aboutForm.moveDarkColorMode();
            }

        }

        /// <summary>
        /// for dark randerer menu
        /// </summary>
        private class DarkRenderer : ToolStripProfessionalRenderer
        {
            protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
            {
                Rectangle rc = new Rectangle(Point.Empty, e.Item.Size);
                Color c = e.Item.Selected ? Color.DarkViolet : Color.Black;
                using (SolidBrush brush = new SolidBrush(c))
                    e.Graphics.FillRectangle(brush, rc);
            }
        }


        /// <summary>
        /// open new window form with explanation on the software and Instructions how to use.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckOpened(Program.GetDescription(Program.FormsName.AboutForm))) { 
                aboutForm = new AboutForm();
                if (isDarkMode)
                    aboutForm.moveDarkColorMode();
                aboutForm.Show();
            }
        }

        /// <summary>
        /// check if window open by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name == name)
                    return true;
            }
            return false;
        }
    }
}
