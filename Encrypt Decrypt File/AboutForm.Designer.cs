﻿namespace Encrypt_Decrypt_File
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.aboutTextBox = new System.Windows.Forms.RichTextBox();
            this.linkToBB = new System.Windows.Forms.LinkLabel();
            this.createLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // aboutTextBox
            // 
            this.aboutTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aboutTextBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.aboutTextBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.aboutTextBox.Location = new System.Drawing.Point(0, 0);
            this.aboutTextBox.Name = "aboutTextBox";
            this.aboutTextBox.ReadOnly = true;
            this.aboutTextBox.Size = new System.Drawing.Size(384, 211);
            this.aboutTextBox.TabIndex = 1;
            this.aboutTextBox.Text = resources.GetString("aboutTextBox.Text");
            // 
            // linkToBB
            // 
            this.linkToBB.AutoSize = true;
            this.linkToBB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkToBB.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkToBB.LinkColor = System.Drawing.Color.DeepSkyBlue;
            this.linkToBB.Location = new System.Drawing.Point(280, 195);
            this.linkToBB.Name = "linkToBB";
            this.linkToBB.Size = new System.Drawing.Size(101, 13);
            this.linkToBB.TabIndex = 2;
            this.linkToBB.TabStop = true;
            this.linkToBB.Text = "Link to source code";
            this.linkToBB.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkToBB_LinkClicked);
            // 
            // createLabel
            // 
            this.createLabel.AutoSize = true;
            this.createLabel.Location = new System.Drawing.Point(2, 195);
            this.createLabel.Margin = new System.Windows.Forms.Padding(0);
            this.createLabel.Name = "createLabel";
            this.createLabel.Size = new System.Drawing.Size(136, 13);
            this.createLabel.TabIndex = 3;
            this.createLabel.Text = "Created by David Yehezkel";
            // 
            // AboutForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(384, 211);
            this.Controls.Add(this.createLabel);
            this.Controls.Add(this.linkToBB);
            this.Controls.Add(this.aboutTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AboutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "About";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.LinkLabel linkToBB;
        private System.Windows.Forms.Label createLabel;
        internal System.Windows.Forms.RichTextBox aboutTextBox;
    }
}